package org.fei.create.model;

import java.util.List;

/**
 * Created by gaolianli on 2015/7/14.
 */
public class TableModel {
    private String name; //����
    private List<FieldModel> fieldModels;
    private Integer fieldNum;//�ֶ���
    private String remark;
    private FieldModel primaryKey; //����

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<FieldModel> getFieldModels() {
        return fieldModels;
    }

    public void setFieldModels(List<FieldModel> fieldModels) {
        this.fieldModels = fieldModels;
    }

    public Integer getFieldNum() {
        return fieldNum;
    }

    public void setFieldNum(Integer fieldNum) {
        this.fieldNum = fieldNum;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public FieldModel getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(FieldModel primaryKey) {
        this.primaryKey = primaryKey;
    }
}
