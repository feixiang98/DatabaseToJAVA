package org.fei.create.service;

import angwei.core.utils.C;
import angwei.core.utils.JsonUtils;
import angwei.core.utils.StringUtils;
import org.fei.create.model.FieldModel;
import org.fei.create.model.TableModel;
import org.fei.create.utils.FieldUtils;
import org.fei.jdbc.JDBCFactory;

import java.sql.*;
import java.util.*;
import java.util.Date;

/**
 * Created by gaolianli on 2015/7/14.
 */
public class FieldService {

    public List<TableModel> getTableModels() throws SQLException {
        List<TableModel> tableModels = new ArrayList<TableModel>();
        Connection conn = JDBCFactory.getInstance();
        DatabaseMetaData meta = conn.getMetaData();
        ResultSet rs = meta.getTables(null, null, null,
                new String[] { "TABLE" });
        while (rs.next()) {
            /*System.out.println("表名：" + rs.getString(3));
            System.out.println("表所属用户名：" + rs.getString(2));*/
            TableModel tableModel = new TableModel();
            tableModel.setName(rs.getString(3));
            this.getFieldModels(tableModel); //添加相关字段信息
            tableModels.add(tableModel);
        }
        return tableModels;
    }

    public void getFieldModels(TableModel tableModel){
        String table = tableModel.getName();
        List<FieldModel> fieldModels = new ArrayList<FieldModel>();
        Connection conn = JDBCFactory.getInstance();
        Statement st = null;
        try {
            DatabaseMetaData dbmd = conn.getMetaData();
            ResultSet rs1 = dbmd.getPrimaryKeys(null,null,table);

            String primaryKeyName = null;

            while(rs1.next()){
                System.out.println("主键：" + rs1.getString(4));
                primaryKeyName = rs1.getString(4);
            }

            rs1.close();

            /*ResultSet rs2 = dbmd.getColumns(null,null,table,null);
            while(rs2.next()){

                System.out.println(rs2.getString(2) +" | "+ rs2.getString(3) +" | "+ rs2.getString(4) +" | "+ rs2.getString(5)
                        +" | "+ rs2.getString(6) +" | "+ rs2.getString(7) +" | "+ rs2.getString(8) +" | "+ rs2.getString(9)
                        +" | "+ rs2.getString(10) +" | "+ rs2.getString(11) +" | "+ rs2.getString(12)
                        +" | "+ rs2.getString(13) +" | "+ rs2.getString(14) +" | "+ rs2.getString(19));

            }*/

            st = conn.createStatement();
            Map<String,String> fc = new HashMap<>();
            ResultSet rs2 = st.executeQuery("show full columns from " + table);
            while(rs2.next()){
                fc.put(rs2.getString("Field"),rs2.getString("Comment"));
            }

            ResultSet rs = st.executeQuery("SELECT * FROM "+table+"");
            ResultSetMetaData metaData = rs.getMetaData();

            for (int i = 0; i < metaData.getColumnCount(); i++) {
                FieldModel fieldModel = new FieldModel();
                fieldModel.setName(metaData.getColumnName(i + 1));
                fieldModel.setType(metaData.getColumnTypeName(i + 1));
                fieldModel.setLength(metaData.getColumnDisplaySize(i + 1) + "");
                fieldModel.setTypeInt(metaData.getColumnType(i + 1));
                fieldModel.setRemark(fc.get(metaData.getColumnName(i + 1)));
                fieldModels.add(fieldModel);

                if(metaData.getColumnName(i + 1).equals(primaryKeyName)){
                    tableModel.setPrimaryKey(fieldModel); // 设为主键
                }

            }

            tableModel.setFieldModels(fieldModels);
            tableModel.setFieldNum(fieldModels.size());

            rs.close();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCFactory.closeConn(conn);
        }
       /* return fieldModels;*/
    }


}
