package org.fei.create.utils;

import org.fei.create.model.TableModel;

public class ActClassUtils {
    public StringBuilder create(TableModel tableModel) {
        StringBuilder s = new StringBuilder();
        ClassUtils classUtils = new ClassUtils();
        String asmImplC = classUtils.nameFormatHandle(tableModel.getName(),1)+Config.ASM_IMPL_SUFFIX;
        String asmC = classUtils.nameFormatHandle(tableModel.getName(),1)+Config.ASM_SUFFIX;
        String bsmC = classUtils.nameFormatHandle(tableModel.getName(),1)+Config.BSM_SUFFIX;
        String dsmC = classUtils.nameFormatHandle(tableModel.getName(),1)+Config.DSM_SUFFIX;
        String pdmC = classUtils.nameFormatHandle(tableModel.getName(),1) + Config.PDM_SUFFIX;

        String asmImpl = Config.ASM_IMPL_PACKAGE + "." + asmImplC;
        String asm = Config.ASM_PACKAGE + "." + asmC;
        String bsm = Config.BSM_PACKAGE + "." + bsmC;
        String dsm = Config.DSM_PACKAGE + "." + dsmC;
        String pdm = Config.PDM_PACKAGE + "." + pdmC;

        //String operatorImport = Config.PDM_PACKAGE + "." + "Operator";
        String operator = "Operator operator";

        s.append("package "+Config.ASM_IMPL_PACKAGE+";\n");
        s.append("import "+pdm+";\nimport "+bsm + Config.BSM_IMPL_SUFFIX + ";\nimport "+asm+";\n");

        //  s.append("import "+operatorImport+";\n");

        for(String ss : Config.BASE_IMPORTS.split(",")){
            s.append("import " + ss + ";\n");
        }
        for(String ss : Config.ASM_IMPORTS.split(",")){
            s.append("import " + ss + ";\n");
        }
        for(String ss : Config.ASM_IMPL_IMPORTS.split(",")){
            s.append("import " + ss + ";\n");
        }

        s.append("\n");

        s.append("@Scope(\"singleton\")\n" +
                "@Repository(\""+classUtils.oneToLowerCase(asmC)+"\")\n" +
                "@Transactional");
        s.append("\npublic class "+ asmImplC+" extends " + bsmC + Config.BSM_IMPL_SUFFIX + " implements "+asmC+" {\n");
        s.append(//"\t@Autowired\n" +
                //"\tprivate "+bsmC+" "+classUtils.oneToLowerCase(bsmC)+";\n" +
                //"\n" +
                "\n" +
                        "\t@Override\n" +
                        "\tpublic int deleteById(Long id, "+operator+") {\n" +
                        "\t\t"+pdmC+ " " + classUtils.oneToLowerCase(pdmC) +" = this.getById(id);\n" +
                        "\t\tint r = this.deleteById("+classUtils.oneToLowerCase(pdmC)+");\n" +
                        "\t\treturn r;\n" +
                        "\t}\n" +
                        "\n" +
                        "\t@Override\n" +
                        "\tpublic int logicDeleteById(Long id, "+operator+") {\n" +
                        "\t\t"+pdmC+ " " + classUtils.oneToLowerCase(pdmC) +" = this.getById(id);\n" +
                        "\t\tif(null == "+classUtils.oneToLowerCase(pdmC)+") throw new DataVerifyException(\"not find\");\n" +
                        "\t\t"+classUtils.oneToLowerCase(pdmC)+".setRowState(-1); //逻辑删除\n"+
                        "\t\tint r = this.logicDeleteById("+classUtils.oneToLowerCase(pdmC)+");\n" +
                        "\t\treturn r;\n" +
                        "\t}\n");
        s.append("\n}");

        return s;
    }
}
