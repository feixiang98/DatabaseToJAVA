package org.fei.create.utils;

import org.fei.create.model.TableModel;

/**
 * Created by gaolianli on 2015/10/23.
 */
public class AsmClassUtils {

    public StringBuilder create(TableModel tableModel){
        StringBuilder s = new StringBuilder();
        ClassUtils classUtils = new ClassUtils();
        String asmC = classUtils.nameFormatHandle(tableModel.getName(),1)+Config.ASM_SUFFIX;
        String bsmC = classUtils.nameFormatHandle(tableModel.getName(),1)+Config.BSM_SUFFIX;
        String dsmC = classUtils.nameFormatHandle(tableModel.getName(),1)+Config.DSM_SUFFIX;
        String pdmC = classUtils.nameFormatHandle(tableModel.getName(),1) + Config.PDM_SUFFIX;


        String bsm = Config.BSM_PACKAGE + "." + bsmC;
        String asm = Config.ASM_PACKAGE + "." + asmC;
        String dsm = Config.DSM_PACKAGE + "." + dsmC;
        String pdm = Config.PDM_PACKAGE + "." + pdmC;
        //String operatorImport = Config.PDM_PACKAGE + "." + "Operator";
        String operator = "Operator operator";

        s.append("package "+Config.ASM_PACKAGE+";\n");
        s.append("import "+pdm+";\n");
        //s.append("import "+operatorImport+";\n");
        s.append("import "+bsm+";\n");
        //s.append("import "+dsm+";\n");

        for(String ss : Config.BASE_IMPORTS.split(",")){
            s.append("import " + ss + ";\n");
        }
        for(String ss : Config.ASM_IMPORTS.split(",")){
            s.append("import " + ss + ";\n");
        }

        s.append("public interface "+ asmC+" extends " + bsmC+"{\n");
        s.append(
                "\t/**\n" +
                "\t* @param id\n" +
                "\t* @return\n" +
                "\t*/\n" +
                "\tint deleteById(Long id, "+operator+"); //\n" +
                "\n" +
                "\t/**\n" +
                "\t* @param id\n" +
                "\t* @return\n" +
                "\t*/\n" +
                "\tint logicDeleteById(Long id, "+operator+");");
        s.append("\n}");

        return s;
    }
}
