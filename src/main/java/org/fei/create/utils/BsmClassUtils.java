package org.fei.create.utils;

import org.fei.create.model.TableModel;

/**
 * Created by gaolianli on 2015/10/23.
 */
public class BsmClassUtils {

    public StringBuilder create(TableModel tableModel){
        StringBuilder s = new StringBuilder();
        ClassUtils classUtils = new ClassUtils();

        String tc = classUtils.nameFormatHandle(tableModel.getName(), 1) + Config.PDM_SUFFIX;

        String bsmC = classUtils.nameFormatHandle(tableModel.getName(),1)+Config.BSM_SUFFIX;
        String dsmC = classUtils.nameFormatHandle(tableModel.getName(),1)+Config.DSM_SUFFIX;
        String pdmC = classUtils.nameFormatHandle(tableModel.getName(),1) + Config.PDM_SUFFIX;
        String bsm = Config.BSM_PACKAGE + "." + bsmC;
        String dsm = Config.DSM_PACKAGE + "." + dsmC;
        String pdm = Config.PDM_PACKAGE + "." + pdmC;

        s.append("package "+Config.BSM_PACKAGE+";\n");
        s.append("import "+pdm+";\n");

        for(String ss : Config.BASE_IMPORTS.split(",")){
            s.append("import " + ss + ";\n");
        }
        for(String ss : Config.BSM_IMPORTS.split(",")){
            s.append("import " + ss + ";\n");
        }

        s.append("public interface "+ classUtils.nameFormatHandle(tableModel.getName(), 1) + Config.BSM_SUFFIX+" {\n");
        s.append("\t/**\n" +
                "\t* @param id\n" +
                "\t* @return\n" +
                "\t*/\n" +
                "\t"+tc+" getById("+FieldUtils.fieldTypeToString(tableModel.getPrimaryKey().getTypeInt())+" id);\n" +
                "\n" +
                "\t/**\n" +
                "\t* @param "+classUtils.oneToLowerCase(tc)+"\n" +
                "\t* @return\n" +
                "\t*/\n" +
                "\tint save("+tc+" "+classUtils.oneToLowerCase(tc)+");\n" +
                "\n" +
                "\t/**\n" +
                "\t* @param "+classUtils.oneToLowerCase(tc)+"\n" +
                "\t* @return\n" +
                "\t*/\n" +
                "\tint update("+tc+" "+classUtils.oneToLowerCase(tc)+");\n" +
                "\n" +
                "\t/**\n" +
                "\t* @return\n" +
                "\t*/\n" +
                "\tint deleteById("+tc+" "+classUtils.oneToLowerCase(tc)+"); //\n" +
                "\n" +
                "\t/**\n" +
                "\t* @return\n" +
                "\t*/\n" +
                "\tint logicDeleteById("+tc+" "+classUtils.oneToLowerCase(tc)+");\n" +
                "\n" +
                "\t/**\n" +
                "\t* @param queryWhere\n" +
                "\t* @return\n" +
                "\t*/\n" +
                "\tList<"+tc+"> getList(QueryWhere queryWhere);\n" +
                "\n" +
                "\t/**\n" +
                "\t* @param queryWhere\n" +
                "\t* @return\n" +
                "\t*/\n" +
                "\tint getCount(QueryWhere queryWhere);\n" +
                "\t/**\n" +
                "\t*\n" +
                "\t* @param queryWhere\n" +
                "\t* @param dbBatch\n" +
                "\t* @return\n" +
                "\t*/\n" +
                "\tList<"+tc+"> getListPaging(QueryWhere queryWhere, DbBatch dbBatch);\n" +
                "\tint logicDeleteById("+FieldUtils.fieldTypeToString(tableModel.getPrimaryKey().getTypeInt())+" id);\n" +
                "\tint deleteById("+FieldUtils.fieldTypeToString(tableModel.getPrimaryKey().getTypeInt())+" id);\n" +
                "");
        s.append("\n}");

        return s;
    }
}
