package org.fei.create.utils;

import org.fei.create.model.TableModel;

/**
 * Created by gaolianli on 2015/10/23.
 */
public class BsmImplClassUtils {

    public StringBuilder create(TableModel tableModel){
        StringBuilder s = new StringBuilder();
        ClassUtils classUtils = new ClassUtils();
        String bsmC = classUtils.nameFormatHandle(tableModel.getName(),1)+Config.BSM_SUFFIX;
        String dsmC = classUtils.nameFormatHandle(tableModel.getName(),1)+Config.DSM_SUFFIX;
        String pdmC = classUtils.nameFormatHandle(tableModel.getName(),1) + Config.PDM_SUFFIX;
        String bsm = Config.BSM_PACKAGE + "." + bsmC;
        String dsm = Config.DSM_PACKAGE + "." + dsmC;
        String pdm = Config.PDM_PACKAGE + "." + pdmC;

        s.append("package "+Config.BSM_IMPL_PACKAGE+";\n");
        s.append("import "+pdm+";\nimport "+dsm+";\nimport "+bsm+";\n");

        for(String ss : Config.BASE_IMPORTS.split(",")){
                s.append("import " + ss + ";\n");
        }
        for(String ss : Config.BSM_IMPORTS.split(",")){
            s.append("import " + ss + ";\n");
        }
        for(String ss : Config.BSM_IMPL_IMPORTS.split(",")){
                s.append("import " + ss + ";\n");
        }


        s.append("@Repository(\""+classUtils.oneToLowerCase(classUtils.nameFormatHandle(tableModel.getName(), 1) + Config.BSM_SUFFIX)+"\")");
        s.append("\n@Transactional");
        s.append("\npublic class "+ classUtils.nameFormatHandle(tableModel.getName(), 1) + Config.BSM_IMPL_SUFFIX+" implements "+classUtils.nameFormatHandle(tableModel.getName(), 1) + Config.BSM_SUFFIX+" {\n");
        s.append("\t@Autowired\n" +
                "\tprivate "+dsmC+" "+classUtils.oneToLowerCase(dsmC)+";\n" +
                "\n" +
                "\n" +
                "\t@Override\n" +
                "\tpublic "+pdmC+" getById("+FieldUtils.fieldTypeToString(tableModel.getPrimaryKey().getTypeInt())+" id) {\n" +
                "\t\treturn this."+classUtils.oneToLowerCase(dsmC)+".selectByPrimaryKey(id);\n" +
                "\t}\n" +
                "\n" +
                "\t@Override\n" +
                "\tpublic int save("+pdmC+" "+classUtils.oneToLowerCase(pdmC)+") {\n" +
                "\t\tif(null == "+classUtils.oneToLowerCase(pdmC)+") throw new NullPointerException(\""+classUtils.oneToLowerCase(pdmC)+" not null\");\n" +
                "\t\tif(null == "+classUtils.oneToLowerCase(pdmC)+".getId()){\n" +
                "\t\t\t"+classUtils.oneToLowerCase(pdmC)+".setId(IdFactory.getNewId());\n" +
                "\t\t}\n" +
                "\t\tint r = 1;\n" +
                "\t\ttry{\n" +
                "\t\t\tthis."+classUtils.oneToLowerCase(dsmC)+".insert("+classUtils.oneToLowerCase(pdmC)+");\n" +
                "\t\t}catch (Exception e){\n" +
                "\t\t\tthrow new DataHandleException(e);\n" +
                "\t\t}\n" +
                "\t\treturn r;\n" +
                "\n" +
                "\t}\n" +
                "\n" +
                "\t@Override\n" +
                "\tpublic int update("+pdmC+" "+classUtils.oneToLowerCase(pdmC)+") {\n" +
                "\t\tif(null == "+classUtils.oneToLowerCase(pdmC)+") throw new NullPointerException(\""+classUtils.oneToLowerCase(pdmC)+" not null\");\n" +
                "\t\tint r = 1;\n" +
                "\t\ttry{\n" +
                "\t\t\tthis."+classUtils.oneToLowerCase(dsmC)+".updateByPrimaryKey("+classUtils.oneToLowerCase(pdmC)+");\n" +
                "\t\t}catch (Exception e){\n" +
                "\t\t\tthrow new DataHandleException(e);\n" +
                "\t\t}\n" +
                "\t\treturn r;\n" +
                "\t}\n" +
                "\n" +
                "\t@Override\n" +
                "\tpublic int deleteById("+pdmC+" "+classUtils.oneToLowerCase(pdmC)+") {\n" +
                "\t\tint r = 1;\n" +
                "\t\ttry{\n" +
                "\t\t\tthis."+classUtils.oneToLowerCase(dsmC)+".deleteByPrimaryKey("+classUtils.oneToLowerCase(pdmC)+".getId());\n" +
                "\t\t}catch (Exception e){\n" +
                "\t\t\tthrow new DataHandleException(e);\n" +
                "\t\t}\n" +
                "\t\treturn r;\n" +
                "\t}\n" +
                "\n" +
                "\t@Override\n" +
                "\tpublic int logicDeleteById("+pdmC+" "+classUtils.oneToLowerCase(pdmC)+") {\n" +
                "\t\tint r = this."+classUtils.oneToLowerCase(dsmC)+".logicDeleteById("+classUtils.oneToLowerCase(pdmC)+".getId());\n" +
               /* "\t\tif(null == "+classUtils.oneToLowerCase(pdmC)+") throw new DataVerifyException(\"not find\");\n" +
                "\t\t"+classUtils.oneToLowerCase(pdmC)+".setRowState(-1); //逻辑删除\n" +
                "\t\tint r = this."+classUtils.oneToLowerCase(dsmC)+".updateByPrimaryKey("+classUtils.oneToLowerCase(pdmC)+");\n" +*/
                "\t\treturn r;\n" +
                "\t}\n" +
                "\n" +
                "\t@Override\n" +
                "\tpublic List<"+pdmC+"> getList(QueryWhere queryWhere) {\n" +
                "\t\tDbBatch dbBatch = new DbBatch(0, queryWhere.getLength());\n" +
                "\t\tif(queryWhere.getLength() == -1){\n" +
                "\t\t\tdbBatch = null;\n" +
                "\t\t}\n"+
                "\t\tList<"+pdmC+"> "+classUtils.oneToLowerCase(pdmC)+"s = this."+classUtils.oneToLowerCase(dsmC)+".queryList(queryWhere.getMap(), dbBatch, queryWhere.getSortMap());\n" +
                "\t\treturn "+classUtils.oneToLowerCase(pdmC)+"s;\n" +
                "\t}\n" +
                "\n" +
                "\t@Override\n" +
                "\tpublic int getCount(QueryWhere queryWhere) {\n" +
                "\t\treturn this."+classUtils.oneToLowerCase(dsmC)+".count(queryWhere.getMap());\n" +
                "\t}\n" +
                "\n" +
                "\t@Override\n" +
                "\tpublic List<"+pdmC+"> getListPaging(QueryWhere queryWhere, DbBatch dbBatch) {\n" +
                "\t\tList<"+pdmC+"> "+classUtils.oneToLowerCase(pdmC)+"s = this."+classUtils.oneToLowerCase(dsmC)+".queryList(queryWhere.getMap(), dbBatch, queryWhere.getSortMap());\n" +
                "\t\treturn "+classUtils.oneToLowerCase(pdmC)+"s;\n" +
                "\t}\n" +
                "\t@Override\n" +
                "\tpublic int logicDeleteById("+FieldUtils.fieldTypeToString(tableModel.getPrimaryKey().getTypeInt())+" id) {\n" +
                "\t\tint r = this."+classUtils.oneToLowerCase(dsmC)+".logicDeleteById(id);\n" +
                /*"\t\t"+pdmC+" "+classUtils.oneToLowerCase(pdmC)+" = this.getById(id);\n" +
                "\t\tif(null == "+classUtils.oneToLowerCase(pdmC)+") throw new DataVerifyException(\"not find\");\n" +
                "\t\t"+classUtils.oneToLowerCase(pdmC)+".setRowState(-1); //逻辑删除\n" +
                "\t\tint r = this.logicDeleteById("+classUtils.oneToLowerCase(pdmC)+");\n" +*/
                "\t\treturn r;\n" +
                "\t}\n" +
                "\n" +
                "\t@Override\n" +
                "\tpublic int deleteById("+FieldUtils.fieldTypeToString(tableModel.getPrimaryKey().getTypeInt())+" id) {\n" +
                "\t\t"+pdmC+" "+classUtils.oneToLowerCase(pdmC)+" = this.getById(id);\n" +
                "\t\tint r = this.deleteById("+classUtils.oneToLowerCase(pdmC)+");\n" +
                "\t\treturn r;\n" +
                "\t}" +
                "");
        s.append("\n}");

        return s;
    }
}
