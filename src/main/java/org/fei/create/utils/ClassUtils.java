package org.fei.create.utils;

import org.fei.create.model.FieldModel;
import org.fei.create.model.TableModel;

/**
 * ����class�����Ϣ
 * Created by gaolianli on 2015/10/22.
 */
public class ClassUtils {

    public StringBuilder createClass(TableModel tableModel){
        StringBuilder s = new StringBuilder();
        s.append("package " + Config.PDM_PACKAGE + ";");

        StringBuilder s2 = new StringBuilder();
        StringBuilder s3 = new StringBuilder();
        for(FieldModel f : tableModel.getFieldModels()){
            /*if("Date".equals(FieldUtils.fieldTypeToString(f.getTypeInt()))){
                s.append("\n\nimport java.util.Date;\n");
            }*/
            createPrivate(s2, f);
            createSetGet(s3, f);
        }
        for(String ss : Config.BASE_IMPORTS.split(",")){
            s.append("\nimport " + ss + ";\n");
        }
        //s.append("import com.textbookpub.types.core.InfoedTdm;\n");

        for(String ss : Config.PDM_IMPORTS.split(",")){
            s.append("import " + ss + ";\n");
        }

        s.append("\npublic class "+ nameFormatHandle(tableModel.getName(), 1) + "Tdm  extends BaseTdm{");
        s.append("\n");
        s.append(s2);
        s.append(s3);
        s.append("\n}");
        return s;
    }

    /**
     * ����className
     * @return
     */
    public void createPrivate(StringBuilder s, FieldModel fieldModel){
        if("money".equals(nameFormatHandle(fieldModel.getName(), 2))){
            System.out.println(fieldModel.getTypeInt());
        }
        s.append("\n\tprivate " + FieldUtils.fieldTypeToString(fieldModel.getTypeInt()));
        s.append(" " + nameFormatHandle(fieldModel.getName(), 2) + ";");
        if(null != fieldModel.getRemark()){
            s.append("\t//" + fieldModel.getRemark());
        }

    }

    public void createSetGet(StringBuilder s, FieldModel fieldModel){
        s.append("\n");
        s.append("\n\tpublic " + FieldUtils.fieldTypeToString(fieldModel.getTypeInt()) + " ");
        s.append("get" + oneUpperCase(nameFormatHandle(fieldModel.getName(),2)) + "(){");
        s.append("\n\t\treturn " + nameFormatHandle(fieldModel.getName(),2) + ";");
        s.append("\n\t}");
        s.append("\n");
        s.append("\n\tpublic void ");
        s.append("set" + oneUpperCase(nameFormatHandle(fieldModel.getName(),2)) + "(" + FieldUtils.fieldTypeToString(fieldModel.getTypeInt()) + " " + nameFormatHandle(fieldModel.getName(),2) + "){");
        s.append("\n\t\tthis." + nameFormatHandle(fieldModel.getName(),2) + " = " + nameFormatHandle(fieldModel.getName(),2) + ";");
        s.append("\n\t}");
        s.append("\n");
    }

    /**
     *
     * @param name
     * @param type  1����ʾ������2�����ֶ���
     * @return
     */
    public String nameFormatHandle(String name, int type){
        StringBuilder r = new StringBuilder();

        String[] names = name.split("_");

        int c = 0; //��ʼλ��
        int e = names.length; //����λ��
        if(type == 1){
            if("true".equals(Config.TABLE_IS_BEFORE)){
                if(Config.TABLE_BEFORES.indexOf(names[c]) >= 0){
                    c = c + 1;
                }
            }

            if("true".equals(Config.TABLE_IS_SUFFIX)){
                if(Config.TABLE_SUFFIXS.indexOf(names[e-1]) >= 0){
                    e = e - 1;
                }
            }
            r.append(names[c].substring(0,1).toUpperCase() + names[c].substring(1));
        }else{

            if("true".equals(Config.IS_BEFORE)){
                if(Config.BEFORES.indexOf(names[c]) >= 0){
                    c = c + 1;
                }
            }

            if("true".equals(Config.IS_SUFFIX)){
                if(Config.SUFFIXS.indexOf(names[e-1]) >= 0){
                    e = e - 1;
                }
            }

            r.append(names[c]);
        }
        for(int i = c + 1; i < e; i++){
            String s = names[i];
            s = s.substring(0,1).toUpperCase() + s.substring(1);
            r.append(s);
        }
        return r.toString();
    }

    public String oneToUpperCase(String name){
        name = name.substring(0,1).toUpperCase() + name.substring(1);
        return name;
    }

    public String oneToLowerCase(String name){
        name = name.substring(0,1).toLowerCase() + name.substring(1);
        return name;
    }

    public void createPackage(StringBuilder s, String className){

    }

    public String oneUpperCase(String name){
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }
}
