package org.fei.create.utils;

import java.util.Properties;

/**
 * Created by gaolianli on 2015/10/22.
 */
public class Config {
    public static String JDBC_TYPE;
    public static String BASE_IMPORTS;

    public static String PDM_PACKAGE;
    public static String PDM_SUFFIX;
    public static String PDM_PATH;
    public static String PDM_IMPORTS;

    public static String DSM_PATH;
    public static String DSM_PACKAGE;
    public static String DSM_IMPORTS;
    public static String DSM_SUFFIX;

    public static String BSM_IMPORTS;
    public static String BSM_PACKAGE;
    public static String BSM_SUFFIX;
    public static String BSM_PATH;

    public static String BSM_IMPL_PACKAGE;
    public static String BSM_IMPL_SUFFIX;
    public static String BSM_IMPL_PATH;
    public static String BSM_IMPL_IMPORTS;

    public static String ASM_PACKAGE;
    public static String ASM_SUFFIX;
    public static String ASM_PATH;
    public static String ASM_IMPORTS;

    public static String ASM_IMPL_PACKAGE;
    public static String ASM_IMPL_SUFFIX;
    public static String ASM_IMPL_PATH;
    public static String ASM_IMPL_IMPORTS;

    public static String XML_PATH;
    public static String IS_BEFORE; //�ֶ��Ƿ���ǰ׺
    public static String BEFORES;   //�ֶ�ǰ׺������Щ
    public static String TABLE_IS_BEFORE;   //�����Ƿ���ǰ׺
    public static String TABLE_BEFORES;     //����ǰ׺����Щ

    public static String IS_SUFFIX; //�ֶ��Ƿ��к�׺
    public static String SUFFIXS;   //�ֶκ�׺������Щ
    public static String TABLE_IS_SUFFIX;   //�����Ƿ��к�׺
    public static String TABLE_SUFFIXS;     //������׺����Щ

    public static String BASE_ITEM_PATH;
    public static String BASE_ITEM_PACKAGE;

    public static void init(Properties prop){

        BASE_ITEM_PACKAGE = prop.getProperty("base.item.package");
        BASE_ITEM_PATH = prop.getProperty("base.item.path");

        JDBC_TYPE = prop.getProperty("jdbc.type").trim();
        PDM_PATH = BASE_ITEM_PATH + prop.getProperty("pdm.path").trim();
        DSM_PATH = BASE_ITEM_PATH + prop.getProperty("dsm.path").trim();
        PDM_PACKAGE = BASE_ITEM_PACKAGE + prop.getProperty("pdm.package").trim();


        BASE_IMPORTS = prop.getProperty("base.imports").trim();
        DSM_IMPORTS = prop.getProperty("dsm.imports").trim();
        PDM_IMPORTS = prop.getProperty("pdm.imports").trim();

        PDM_SUFFIX = prop.getProperty("pdm.suffix").trim();
        DSM_SUFFIX = prop.getProperty("dsm.suffix").trim();
        DSM_PACKAGE = BASE_ITEM_PACKAGE + prop.getProperty("dsm.package").trim();
        XML_PATH = BASE_ITEM_PATH + prop.getProperty("xml.path").trim();

        BSM_SUFFIX = prop.getProperty("bsm.suffix").trim();
        BSM_PACKAGE = BASE_ITEM_PACKAGE + prop.getProperty("bsm.package").trim();
        BSM_PATH = BASE_ITEM_PATH + prop.getProperty("bsm.path").trim();
        BSM_IMPORTS = prop.getProperty("bsm.imports").trim();

        BSM_IMPL_SUFFIX = prop.getProperty("bsm.impl.suffix").trim();
        BSM_IMPL_PACKAGE = BASE_ITEM_PACKAGE + prop.getProperty("bsm.impl.package").trim();
        BSM_IMPL_PATH = BASE_ITEM_PATH + prop.getProperty("bsm.impl.path").trim();
        BSM_IMPL_IMPORTS = prop.getProperty("bsm.impl.imports").trim();

        ASM_SUFFIX = prop.getProperty("asm.suffix").trim();
        ASM_PACKAGE = BASE_ITEM_PACKAGE + prop.getProperty("asm.package").trim();
        ASM_PATH = BASE_ITEM_PATH + prop.getProperty("asm.path").trim();
        ASM_IMPORTS = prop.getProperty("asm.imports").trim();

        ASM_IMPL_SUFFIX = prop.getProperty("asm.impl.suffix").trim();
        ASM_IMPL_PACKAGE = BASE_ITEM_PACKAGE + prop.getProperty("asm.impl.package").trim();
        ASM_IMPL_PATH = BASE_ITEM_PATH + prop.getProperty("asm.impl.path").trim();
        ASM_IMPL_IMPORTS = prop.getProperty("asm.impl.imports").trim();

        IS_BEFORE = prop.getProperty("isBefore").trim();
        BEFORES = prop.getProperty("befores").trim();
        TABLE_IS_BEFORE = prop.getProperty("tableIsBefore").trim();
        TABLE_BEFORES = prop.getProperty("tableBefores").trim();

        IS_SUFFIX = prop.getProperty("isSuffix").trim();
        TABLE_IS_SUFFIX = prop.getProperty("tableIsSuffix").trim();
        TABLE_SUFFIXS = prop.getProperty("tableSuffixs").trim();
        SUFFIXS = prop.getProperty("suffixs").trim();

    }
}
