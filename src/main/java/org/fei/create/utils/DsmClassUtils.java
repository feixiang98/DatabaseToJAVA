package org.fei.create.utils;

import org.fei.create.model.TableModel;

/**
 * Created by gaolianli on 2015/10/23.
 */
public class DsmClassUtils {

    public StringBuilder create(TableModel tableModel){
        StringBuilder s = new StringBuilder();
        ClassUtils classUtils = new ClassUtils();
        String c = Config.DSM_PACKAGE + "." + classUtils.nameFormatHandle(tableModel.getName(),1)+Config.DSM_SUFFIX;
        String cc = Config.PDM_PACKAGE + "." + classUtils.nameFormatHandle(tableModel.getName(),1) + Config.PDM_SUFFIX;
        String tc = classUtils.nameFormatHandle(tableModel.getName(), 1) + Config.PDM_SUFFIX;
        s.append("package "+Config.DSM_PACKAGE+";\n");
        s.append("import "+cc+";\n" +
                "import org.apache.ibatis.annotations.Param;\n");
        for(String ss : Config.BASE_IMPORTS.split(",")){
            s.append("import " + ss + ";\n");
        }
        for(String ss : Config.DSM_IMPORTS.split(",")){
            s.append("import " + ss + ";\n");
        }

        s.append("public interface "+ classUtils.nameFormatHandle(tableModel.getName(), 1) + Config.DSM_SUFFIX+" {\n");
        s.append("\tint deleteByPrimaryKey("+FieldUtils.fieldTypeToString(tableModel.getPrimaryKey().getTypeInt())+" id);\n" +
                "\n" +
                "\tint insert("+tc+" record);\n" +
                "\n" +
                "\tint insertSelective("+tc+" record);\n" +
                "\n" +
                "\t"+tc+" selectByPrimaryKey("+FieldUtils.fieldTypeToString(tableModel.getPrimaryKey().getTypeInt())+" id);\n" +
                "\n" +
                "\tint updateByPrimaryKeySelective("+tc+" record);\n" +
                "\n" +
                "\tint updateByPrimaryKey("+tc+" record);\n" +
                "\n" +
                "\tint logicDeleteById("+FieldUtils.fieldTypeToString(tableModel.getPrimaryKey().getTypeInt())+" id);\n" +
                "\n" +
                "\t"+tc+" getByCode(String code);\n");

        s.append("\tint count(@Param(value = \"map\") Map map);\n" +
                "\n" +
                "\tList<"+tc+"> queryList(@Param(value = \"map\") Map map, @Param(value = \"dbBatch\") DbBatch dbBatch, @Param(value = \"sortmap\") Map sortmap);");
        s.append("\n}");

        return s;
    }
}
