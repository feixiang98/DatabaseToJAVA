package org.fei.create.utils;

import java.sql.Types;

/**
 * Created by gaolianli on 2015/7/14.
 */
public class FieldUtils {

    public static String fieldTypeToString(int type){
        String typeString = "";
        switch (type){
            case Types.BIGINT :
                typeString = "Long";
                break;
            case Types.VARCHAR:
                typeString = "String";
                break;
            case Types.CHAR:
                typeString = "String";
                break;
            case Types.LONGVARCHAR:
                typeString = "String";
                break;
            case Types.DATE:
                typeString = "Date";
                break;
            case Types.TIMESTAMP:
                typeString = "Date";
                break;
            case Types.INTEGER:
                typeString = "Integer";
                break;
            case Types.TINYINT:
                typeString = "Integer";
                break;
            case Types.SMALLINT:
                typeString = "Integer";
                break;
            case Types.BIT:
                typeString = "Integer";
                break;
            case Types.NUMERIC:
                typeString = "Double";
                break;
            case Types.DECIMAL:
                typeString = "Double";
                break;
            case Types.FLOAT:
                typeString = "Float";
                break;
            case Types.REAL:
                typeString = "Float";
                break;
            case Types.DOUBLE:
                typeString = "Double";
                break;
            default:
                System.out.println("没有找到 type:" + type);
                break;
        }
        return typeString;
    }

    public static String toJdbcTypeString(int type){
        String typeString = "";
        switch (type){
            case Types.BIGINT :
                typeString = "BIGINT";
                break;
            case Types.VARCHAR:
                typeString = "VARCHAR";
                break;
            case Types.CHAR:
                typeString = "VARCHAR";
                break;
            case Types.LONGVARCHAR:
                typeString = "VARCHAR";
                break;
            case Types.DATE:
                typeString = "DATE";
                break;
            case Types.TIMESTAMP:
                typeString = "TIMESTAMP";
                break;
            case Types.INTEGER:
                typeString = "INTEGER";
                break;
            case Types.TINYINT:
                typeString = "INTEGER";
                break;
            case Types.SMALLINT:
                typeString = "INTEGER";
                break;
            case Types.BIT:
                typeString = "INTEGER";
                break;
            case Types.NUMERIC:
                typeString = "NUMERIC";
                break;
            case Types.DECIMAL:
                typeString = "NUMERIC";
                break;
            case Types.FLOAT:
                typeString = "FLOAT";
                break;
            case Types.REAL:
                typeString = "FLOAT";
                break;
            case Types.DOUBLE:
                typeString = "DOUBLE";
                break;
            default:
                System.out.println("没有找到 type:" + type);
                break;
        }
        return typeString;
    }


    public static String fieldTypePackageToString(int type){
        String typeString = "";
        switch (type){
            case Types.BIGINT :
                typeString = "java.lang.Long";
                break;
            case Types.VARCHAR:
                typeString = "java.lang.String";
                break;
            case Types.CHAR:
                typeString = "java.lang.String";
                break;
            case Types.DATE:
                typeString = "java.util.Date";
                break;
            case Types.TIMESTAMP:
                typeString = "java.util.Date";
                break;
            case Types.INTEGER:
                typeString = "java.lang.Integer";
                break;
            case Types.TINYINT:
                typeString = "java.lang.Integer";
                break;
            case Types.SMALLINT:
                typeString = "java.lang.Integer";
                break;
            case Types.BIT:
                typeString = "java.lang.Integer";
                break;
            case Types.NUMERIC:
                typeString = "java.lang.Double";
                break;
            case Types.FLOAT:
                typeString = "java.lang.Float";
                break;
            case Types.REAL:
                typeString = "java.lang.Float";
                break;
            case Types.DOUBLE:
                typeString = "java.lang.Double";
                break;
            default:
                System.out.println("没有找到 type:" + type);
                break;
        }
        return typeString;
    }
}
