package org.fei.create.utils;

import org.fei.create.model.FieldModel;
import org.fei.create.model.TableModel;

/**
 * Created by gaolianli on 2015/10/22.
 */
public class MapperUtils {

    public StringBuilder createMapper(TableModel tableModel){
        StringBuilder s = new StringBuilder();
        ClassUtils classUtils = new ClassUtils();
        String c = Config.DSM_PACKAGE + "." + classUtils.nameFormatHandle(tableModel.getName(),1)+Config.DSM_SUFFIX;
        String cc = Config.PDM_PACKAGE + "." + classUtils.nameFormatHandle(tableModel.getName(),1) + Config.PDM_SUFFIX;
        String defMap = classUtils.nameFormatHandle(tableModel.getName(),1) + "Map";
        s.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n" +
                "<!DOCTYPE mapper PUBLIC \"-//mybatis.org//DTD Mapper 3.0//EN\" \"http://mybatis.org/dtd/mybatis-3-mapper.dtd\" >\n" +
                "<mapper namespace=\""+ c +"\" >");
        s.append("\n<resultMap id=\""+defMap+"\" type=\""+cc+"\" >\n" +
                "\n\t<id column=\""+tableModel.getPrimaryKey().getName()+"\" property=\""+classUtils.nameFormatHandle(tableModel.getPrimaryKey().getName(),2)+"\" jdbcType=\""+FieldUtils.toJdbcTypeString(tableModel.getPrimaryKey().getTypeInt())+"\" />");
        StringBuilder s1 = new StringBuilder();
        StringBuilder s2 = new StringBuilder();
        StringBuilder s3 = new StringBuilder();
        StringBuilder s4 = new StringBuilder();
        StringBuilder s5 = new StringBuilder();
        StringBuilder s6 = new StringBuilder();
        for(FieldModel f : tableModel.getFieldModels()){
            s.append("\n\t<result column=\""+f.getName()+"\" property=\""+classUtils.nameFormatHandle(f.getName(),2)+"\" jdbcType=\""+FieldUtils.toJdbcTypeString(f.getTypeInt())+"\" />");
            s1.append(f.getName());
            s1.append(",");
            s2.append("#{"+classUtils.nameFormatHandle(f.getName(),2)+",jdbcType="+FieldUtils.toJdbcTypeString(f.getTypeInt())+"}");
            s2.append(",");

            s3.append("\n\t\t<if test=\""+classUtils.nameFormatHandle(f.getName(),2)+" != null\" >\n" +
                    "\t\t\t"+f.getName()+",\n" +
                    "\t\t</if>");

            s4.append("\n\t\t<if test=\""+classUtils.nameFormatHandle(f.getName(),2)+" != null\" >");
            s4.append("\n\t\t\t#{"+classUtils.nameFormatHandle(f.getName(),2)+",jdbcType="+FieldUtils.toJdbcTypeString(f.getTypeInt())+"}");
            s4.append("\n\t\t</if>");

            s5.append("\n\t\t<if test=\""+classUtils.nameFormatHandle(f.getName(),2)+" != null\" >");
            s5.append("\n\t\t\t"+f.getName()+" = #{"+classUtils.nameFormatHandle(f.getName(),2)+",jdbcType="+FieldUtils.toJdbcTypeString(f.getTypeInt())+"},");
            s5.append("\n\t\t</if>");

            s6.append("\n\t\t\t"+f.getName()+" = #{"+classUtils.nameFormatHandle(f.getName(),2)+",jdbcType="+FieldUtils.toJdbcTypeString(f.getTypeInt())+"},");

        }
        s.append("\n\t</resultMap>");

        //���������ѯ

        s.append("\n\t<sql id=\"Base_Column_List\" >");
        s.append("\n\t\t"+s1.toString().substring(0,s1.toString().length()-1));
        s.append("\n\t</sql>");

        s.append("\n\t<select id=\"selectByPrimaryKey\" resultMap=\""+defMap+"\" parameterType=\""+FieldUtils.fieldTypePackageToString(tableModel.getPrimaryKey().getTypeInt())+"\" >\n" +
                "\t\tselect \n" +
                "\t\t<include refid=\"Base_Column_List\" />\n" +
                "\t\tfrom "+tableModel.getName()+"\n" +
                "\t\twhere "+tableModel.getPrimaryKey().getName()+" = #{"+classUtils.nameFormatHandle(tableModel.getPrimaryKey().getName(),2)+",jdbcType="+FieldUtils.toJdbcTypeString(tableModel.getPrimaryKey().getTypeInt())+"}\n" +
                "\t</select>");

        s.append("\n" +
                "\t<delete id=\"deleteByPrimaryKey\" parameterType=\""+FieldUtils.fieldTypePackageToString(tableModel.getPrimaryKey().getTypeInt())+"\" >\n" +
                "\t\tdelete from "+tableModel.getName()+"\n" +
                "\t\twhere "+tableModel.getPrimaryKey().getName()+" = #{"+classUtils.nameFormatHandle(tableModel.getPrimaryKey().getName(),2)+",jdbcType="+FieldUtils.toJdbcTypeString(tableModel.getPrimaryKey().getTypeInt())+"}\n" +
                "\t</delete>");


        s.append("\n\t<insert id=\"insert\" parameterType=\""+cc+"\"  useGeneratedKeys=\"true\" keyProperty=\""+classUtils.nameFormatHandle(tableModel.getPrimaryKey().getName(),2)+"\">\n" +
                "\t\tinsert into "+tableModel.getName()+" ("+
                s1.toString().substring(0,s1.toString().length()-1)
                +")\n" +
                "\t\tvalues (" +
                s2.toString().substring(0,s2.toString().length()-1)
                +")\n" +
                "\t</insert>");

        s.append("\n\t<insert id=\"insertSelective\" parameterType=\""+cc+"\" >\n" +
                "\t\tinsert into "+tableModel.getName()+"\n");

        s.append("\t\t<trim prefix=\"(\" suffix=\")\" suffixOverrides=\",\" >");

        s.append(""+
                s3.toString()
                +"\n");
        s.append("\t\t</trim>\n" +
                "\t\t<trim prefix=\"values (\" suffix=\")\" suffixOverrides=\",\" >");

        s.append("\t\t\t" +
                s4.toString()
                +"\n");
        s.append("\t\t</trim>\n"+
                "\t</insert>");

        s.append("\n\t<update id=\"updateByPrimaryKeySelective\" parameterType=\""+cc+"\" >\n" +
                "\t\tupdate "+tableModel.getName()+"\n"+
                "\t\t<set >");
        s.append(s5);
        s.append("\n\t\t</set>");
        s.append("\n\t\twhere "+tableModel.getPrimaryKey().getName()+" = #{"+classUtils.nameFormatHandle(tableModel.getPrimaryKey().getName(),2)+",jdbcType="+FieldUtils.toJdbcTypeString(tableModel.getPrimaryKey().getTypeInt())+"}");
        s.append("\n\t</update>");

        s.append("\n\t<update id=\"updateByPrimaryKey\" parameterType=\""+cc+"\" >\n" +
                "\t\tupdate "+tableModel.getName()+"\n"+
                "\t\t set ");

        s.append(s6.toString().substring(0,s6.toString().length()-1));

        s.append("");
        s.append("\n\t\twhere "+tableModel.getPrimaryKey().getName()+" = #{"+classUtils.nameFormatHandle(tableModel.getPrimaryKey().getName(),2)+",jdbcType="+FieldUtils.toJdbcTypeString(tableModel.getPrimaryKey().getTypeInt())+"}");
        s.append("\n\t</update>");


        s.append("\n\t<select id=\"count\" resultType=\"int\" flushCache=\"false\" useCache=\"true\">\n" +
                "\t\tselect\n" +
                "\t\tcount(*) as num\n" +
                "\t\tfrom "+tableModel.getName()+"\n" +
                "\t\twhere 1=1\n" +
                "\t\t<include refid=\"query_where\"/>\n" +
                "\t</select>");

        s.append("\n\t<!--列表-->\n" +
                "\t<select id=\"queryList\" resultMap=\""+defMap+"\" flushCache=\"false\" useCache=\"true\">\n" +
                "\t\tselect\n" +
                "\t\t<include refid=\"Base_Column_List\"/>\n" +
                "\t\tfrom "+tableModel.getName()+"\n" +
                "\t\twhere 1=1\n" +
                "\t\t<include refid=\"query_where\"/>\n" +
                "\t\t<include refid=\"query_sort\"/>\n" +
                "\t\t<if test=\"dbBatch != null\" >\n");
                if(Config.JDBC_TYPE.equals("mysql")){
                    s.append("\t\t\tlimit #{dbBatch.offset, jdbcType=INTEGER}, #{dbBatch.batchSize, jdbcType=INTEGER}\n");
                }
                if(Config.JDBC_TYPE.equals("postgresql")){
                    s.append("\t\t\tlimit #{dbBatch.batchSize, jdbcType=INTEGER} OFFSET #{dbBatch.offset, jdbcType=INTEGER}\n");
                }
        s.append("\t\t</if>\n" +
                "\n" +
                "\t</select>");

        s.append("<sql id=\"query_sort\">\n" +
                "\t\t<if test=\"sortmap != null\">\n" +
                "\t\t\torder by\n" +
                "\t\t\t<foreach collection=\"sortmap\" index=\"key\"  item=\"ent\" separator=\"\">\n" +
                "\t\t\t\t${key} ${ent},\n" +
                "\t\t\t</foreach>\n" +
                "\t\t\tid desc\n" +
                "\t\t</if>\n" +
                "\n" +
                "\n" +
                "\t</sql>\n" +
                "\t<sql id=\"query_where\">\n" +
               /* "\t\t<if test=\"map == null\">\n" +
                "\t\t\tand row_state=0\n" +
                "\t\t</if>\n" +
                "\t\t<if test=\"map != null\">\n" +
                "\t\t\t<if test=\"map.rowState != null\">\n" +
                "\t\t\t\tand row_state=#{map.rowState}\n" +
                "\t\t\t</if>\n" +
                "\t\t\t<if test = \"map.rowState == null \">\n" +
                "\t\t\t\tand row_state=0\n" +
                "\t\t\t</if>\n" +
                "\t\t</if>\n" +
                "\n" +*/
                "\t</sql>");

        s.append("\n\t<select id=\"getByCode\" resultMap=\""+defMap+"\">\n" +
                "\t\tselect \n" +
                "\t\t<include refid=\"Base_Column_List\" />\n" +
                "\t\tfrom "+tableModel.getName()+"\n" +
                "\t\twhere code = #{code} and row_state=0\n" +
                "\t</select>");

        s.append("\n\t<update id=\"logicDeleteById\" >\n" +
                "\t\tupdate "+tableModel.getName()+"\n" +
                "\t\tset row_state = -1 \n" +
                "\t\twhere id = #{id} and row_state=0\n" +
                "\t</update>");
        s.append("\n</mapper>");
        return s;
    }
}
