package org.fei.create.utils;

import java.io.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

/**
 * 类型转换工具
 * @author gaoll
 * @date 2015年5月15日 上午10:49:31
 */
public class TypeChange {



	/**
	 * 将字节数组转换成16进制字符串
	 * @param byteArray 要转码的字节数组
	 * @return 返回转码后的16进制字符串
	 */
	public static String byteArrayToHexStr(byte byteArray[]) {
		StringBuffer buffer = new StringBuffer(byteArray.length * 2);
		int i;
		for (i = 0; i < byteArray.length; i++) {
			if (((int) byteArray[i] & 0xff) < 0x10)//小于十前面补零
				buffer.append("0");
			buffer.append(Long.toString((int) byteArray[i] & 0xff, 16));
		}
		return buffer.toString();
	}

	/**
	 * 将16进制字符串转换成字节数组
	 * @param hexStr 要转换的16进制字符串
	 * @return 返回转码后的字节数组
	 */
	public static byte[] hexStrToByteArray(String hexStr) {
		if (hexStr.length() < 1)
			return null;
		byte[] encrypted = new byte[hexStr.length() / 2];
		for (int i = 0; i < hexStr.length() / 2; i++) {
			int high = Integer.parseInt(hexStr.substring(i * 2, i * 2 + 1), 16);//取高位字节
			int low = Integer.parseInt(hexStr.substring(i * 2 + 1, i * 2 + 2), 16);//取低位字节
			encrypted[i] = (byte) (high * 16 + low);
		}
		return encrypted;
	}

	/**
	 * 将一个文件转换成字节数组
	 * @param file
	 * @return 字节数组
	 */
	public static byte[] fileToByteArray(File file){
		// 非法参数.
		if(file==null){
			return new byte[0];
		}

		// 如果不存在或者是目录.
		if(!file.exists() || file.isDirectory()){
			return new byte[0];
		}

		// 加载文件大小匹配的缓冲区.
		long fileSize = file.length();
		InputStream is = null;
		byte[] buffer = new byte[(int) fileSize];

		try {
			is = new FileInputStream(file);
			is.read(buffer);

//			byte[] temp = new byte[100];
//			int offset = 0;
//			int numRead = 0;
//			while (offset < buffer.length
//					&& (numRead = is.read(buffer, offset, buffer.length
//					- offset)) >= 0) {
//				offset += numRead;
//			}
//
//			// 确保所有数据均被读取
//			if (offset != buffer.length) {
//				throw new IOException("Could not completely read file "
//						+ file.getName());
//			}


		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//			throw new RuntimeException("文件没找到！");
		} catch (IOException e) {
//			e.printStackTrace();
//			throw new RuntimeException("读取文件错误！");
		}finally{
			if(null != is){
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return buffer;
	}

	/**
	 * byte[] 保存到文件，如该文件已存在则替换
	 * @param data
	 * @param fileUrl
	 * @return 1成功，0失败
	 */
	public static int ByteArrayToFile(byte[] data, File fileUrl){
		int result = 0;
		OutputStreamWriter os = null;
		FileOutputStream fs = null;
		try {
			fs = new FileOutputStream(fileUrl);
			os = new OutputStreamWriter(fs,"UTF-8");

			os.write(new String(data));
			result = 1;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (IOException e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}finally{
			if(os != null){
				try {
					os.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}

	public static String mimeTypeToExt(String subType){
		String result = null;
		switch (subType){
			case "png":
				break;
			case "":
				break;
		}
		return result;
	}

	public static String relativePathToAbsolutePath(String relativePath){
		return null;
	}

	public static Certificate byteArrayToCert(byte[] bodys){
		try {
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			Certificate certificate = cf.generateCertificate(new ByteArrayInputStream(bodys));
			return certificate;
		} catch (CertificateException e) {
			e.printStackTrace();
		}
		return null;
	}
}
