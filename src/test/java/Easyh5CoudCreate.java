import angwei.core.utils.C;
import org.fei.create.model.TableModel;
import org.fei.create.service.FieldService;
import org.fei.create.utils.*;
import org.fei.jdbc.JDBCFactory;
import org.junit.Test;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by gaolianli on 2015/10/22.
 */
public class Easyh5CoudCreate {
    private void init(){
        JDBCFactory.init("/config-easyh5.properties");
    }

    @Test
    public void create() throws SQLException{
        init();
        createClassModel();
        System.out.println("MODEL完成");
        createDSM();
        System.out.println("DSM完成");
        createXML();
        System.out.println("MAPPER完成");
        createBSM();
        System.out.println("BSM&ASM完成");
        System.out.println("全部完成");
    }

    public void createClassModel() throws SQLException {
        ClassUtils classUtils = new ClassUtils();

        FieldService fs = new FieldService();
        List<TableModel> tableModels = fs.getTableModels();
        File file = new File(Config.PDM_PATH);
        file.mkdirs();
        for(TableModel t : tableModels){
            StringBuilder s = classUtils.createClass(t);
            TypeChange.ByteArrayToFile(s.toString().getBytes(), new File(Config.PDM_PATH + "/" + classUtils.nameFormatHandle(t.getName(),1) + Config.PDM_SUFFIX + ".java"));
        }


    }



     public void createXML() throws SQLException {
        ClassUtils classUtils = new ClassUtils();
        MapperUtils mapperUtils = new MapperUtils();
        FieldService fs = new FieldService();
        List<TableModel> tableModels = fs.getTableModels();
        File file = new File(Config.XML_PATH);
        file.mkdirs();
        for(TableModel t : tableModels){
            //System.out.println("MAPPER:" + t.getName());
            StringBuilder s = mapperUtils.createMapper(t);
            TypeChange.ByteArrayToFile(s.toString().getBytes(), new File(Config.XML_PATH + "/" + classUtils.nameFormatHandle(t.getName(),1) + Config.DSM_SUFFIX+ ".xml"));
        }
    }


    public void createDSM() throws SQLException {
        ClassUtils classUtils = new ClassUtils();
        DsmClassUtils dsmClassUtils = new DsmClassUtils();
        FieldService fs = new FieldService();
        List<TableModel> tableModels = fs.getTableModels();
        File file = new File(Config.DSM_PATH);
        file.mkdirs();
        for(TableModel t : tableModels){
            C.println("表：" + t.getName());
            StringBuilder s = dsmClassUtils.create(t);
            TypeChange.ByteArrayToFile(s.toString().getBytes(), new File(Config.DSM_PATH + "/" + classUtils.nameFormatHandle(t.getName(),1) + Config.DSM_SUFFIX + ".java"));
        }
    }


    public void createBSM() throws SQLException {
        ClassUtils classUtils = new ClassUtils();
        DsmClassUtils dsmClassUtils = new DsmClassUtils();

        BsmClassUtils bsmClassUtils = new BsmClassUtils();
        BsmImplClassUtils bsmImplClassUtils = new BsmImplClassUtils();

        AsmClassUtils asmClassUtils = new AsmClassUtils();
        AsmImplClassUtils asmImplClassUtils = new AsmImplClassUtils();

        FieldService fs = new FieldService();
        List<TableModel> tableModels = fs.getTableModels();
        //File file = new File(Config.DSM_PATH);
        //file.mkdirs();

        File file2 = new File(Config.BSM_PATH);
        file2.mkdirs();

        File file3 = new File(Config.BSM_IMPL_PATH);
        file3.mkdirs();

        File file4 = new File(Config.ASM_PATH);
        file4.mkdirs();

        File file5 = new File(Config.ASM_IMPL_PATH);
        file5.mkdirs();

        for(TableModel t : tableModels){
           // StringBuilder s = dsmClassUtils.create(t);
            StringBuilder s2 = bsmClassUtils.create(t);

            StringBuilder s3 = bsmImplClassUtils.create(t);
            StringBuilder s4 = asmClassUtils.create(t);
            StringBuilder s5 = asmImplClassUtils.create(t);
            //TypeChange.ByteArrayToFile(s.toString().getBytes(), new File(Config.DSM_PATH + "/" + classUtils.nameFormatHandle(t.getName(),1) + Config.DSM_SUFFIX + ".java"));

            TypeChange.ByteArrayToFile(s2.toString().getBytes(), new File(Config.BSM_PATH + "/" + classUtils.nameFormatHandle(t.getName(),1) + Config.BSM_SUFFIX + ".java"));
            TypeChange.ByteArrayToFile(s3.toString().getBytes(), new File(Config.BSM_IMPL_PATH + "/" + classUtils.nameFormatHandle(t.getName(),1) + Config.BSM_IMPL_SUFFIX + ".java"));

            //暂时先不生成ASM
            /*TypeChange.ByteArrayToFile(s4.toString().getBytes(), new File(Config.ASM_PATH + "/" + classUtils.nameFormatHandle(t.getName(),1) + Config.ASM_SUFFIX + ".java"));
            TypeChange.ByteArrayToFile(s5.toString().getBytes(), new File(Config.ASM_IMPL_PATH + "/" + classUtils.nameFormatHandle(t.getName(),1) + Config.ASM_IMPL_SUFFIX + ".java"));*/
        }
    }
}
