import org.fei.create.model.FieldModel;
import org.fei.create.model.TableModel;
import org.fei.create.model.User;
import org.fei.create.service.FieldService;
import org.fei.create.utils.ClassUtils;
import org.fei.create.utils.Config;
import org.junit.Test;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by gaolianli on 2015/7/14.
 */
public class Test01 {

    @Test
    public void testField() throws SQLException {
        ClassUtils classUtils = new ClassUtils();
        FieldService fs = new FieldService();
        List<TableModel> tableModels = fs.getTableModels();
        for(TableModel t : tableModels){
            System.out.println("表名：" + classUtils.nameFormatHandle(t.getName(), 1));
            for(FieldModel f : t.getFieldModels()){
                System.out.println(f.getName() +" | "+ f.getType() + "|" + f.getTypeInt() +" | "+ f.getLength()+" | "+ f.getDefaultValue());
            }
            System.out.println("-------------------------------------------------");
        }

        //System.out.println(Config.JDBC_TYPE);
    }

/*    @Test
    public void testClass(){
        Class c1 = null;
        try {
            c1 = Class.forName("org.fei.create.model.User");
            User user = (User)c1.newInstance();


        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }*/


   /* @Test
    public void testAsm(){
        //����һ����ֻ��ҪClassWriter�������
        ClassWriter cw = new ClassWriter(0);
        //ͨ��visit����ȷ�����ͷ����Ϣ
        cw.visit(Opcodes.V1_5, Opcodes.ACC_PUBLIC+Opcodes.ACC_ABSTRACT+Opcodes.ACC_INTERFACE,
                "com/asm3/Comparable", null, "java/lang/Object", new String[]{"com/asm3/Mesurable"});
         //�����������
        cw.visitField(Opcodes.ACC_PUBLIC+Opcodes.ACC_FINAL+Opcodes.ACC_STATIC,
                "LESS", "I", null, new Integer(-1)).visitEnd();
        cw.visitField(Opcodes.ACC_PUBLIC+Opcodes.ACC_FINAL+Opcodes.ACC_STATIC,
                "EQUAL", "I", null, new Integer(0)).visitEnd();
        cw.visitField(Opcodes.ACC_PUBLIC+Opcodes.ACC_FINAL+Opcodes.ACC_STATIC,
                "GREATER", "I", null, new Integer(1)).visitEnd();
        //������ķ���
        cw.visitMethod(Opcodes.ACC_PUBLIC+ Opcodes.ACC_ABSTRACT, "compareTo",
                "(Ljava/lang/Object;)I", null, null).visitEnd();
        cw.visitEnd(); //ʹcw���Ѿ����
        //��cwת�����ֽ�����д���ļ�����ȥ
        byte[] data = cw.toByteArray();
        File file = new File("D://Comparable.class");
        FileOutputStream fout = null;
        try {
            fout = new FileOutputStream(file);
            fout.write(data);
            fout.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }*/
}
